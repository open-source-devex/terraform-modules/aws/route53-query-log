terraform {
  backend "remote" {
    organization = "open-source-devex"
    workspaces {
      name = "terraform-modules-aws-route53-query-log-complete"
    }
  }
}

provider "aws" {
  region = "eu-west-1"
}

provider "aws" {
  region = "us-east-1"

  alias = "us_east_1"
}

data "aws_caller_identity" "current" {}

module "route53_query_log" {
  source = "../../"

  route53_zone_id = aws_route53_zone.test.zone_id

  providers = {
    aws           = aws
    aws.us_east_1 = aws.us_east_1,
  }
}

resource "aws_route53_zone" "test" {
  name = "test.my.zone.com"
}

output "cloudwatch_log_group_id" {
  value = module.route53_query_log.cloudwatch_log_group_id
}

output "kms_key_arn" {
  value = module.route53_query_log.kms_key_arn
}
