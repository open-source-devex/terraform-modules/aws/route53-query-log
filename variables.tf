variable project {
  type    = string
  default = "prj"
}

variable environment {
  type    = string
  default = "env"
}

variable "tags" {
  description = "Tags to be added to resources that support it"
  type        = map(string)
  default     = {}
}

variable "route53_zone_id" {
  description = "The Route53 zone for which to enable query logging"
  type        = string
}

variable "dns_log_retention_in_days" {
  description = "For how many days should CloudWatch retain logs"
  type        = number
  default     = 60
}

variable "kms_key_arn" {
  description = "The ARN of a pre-existing KMS key to use for encrypting logs, leave empty to disable encryption"
  type        = string
  default     = ""
}

variable "encrypt_logs" {
  description = "Whether to encrypt logs with a provided key (kms_key_arn) or a new one. When set kms_key_arn, that key will take precedence"
  type        = bool
  default     = true
}

variable "enabled" {
  description = "Whether to create the resources in the module"
  type        = bool
  default     = true
}
