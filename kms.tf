resource "aws_kms_key" "default" {
  count = local.create_new_kms_key ? 1 : 0

  description = "Encrypt Route53 query logs for ${local.resource_name_suffix}"

  enable_key_rotation = true

  policy = join("", data.aws_iam_policy_document.kms.*.json)

  tags = var.tags

  provider = aws.us_east_1
}

data "aws_iam_policy_document" "kms" {
  count = local.create_new_kms_key ? 1 : 0

  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["logs.us-east-1.amazonaws.com"]
    }

    actions = [
      "kms:Encrypt*",
      "kms:Decrypt*",
      "kms:ReEncrypt*",
      "kms:GenerateDataKey*",
      "kms:Describe*",
    ]

    resources = ["*"]
  }

  statement {
    sid = "KeyAdministration"

    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${local.current_aws_account_id}:root"]
    }

    effect = "Allow"

    actions = [
      "kms:Create*",
      "kms:Describe*",
      "kms:Enable*",
      "kms:List*",
      "kms:Put*",
      "kms:Update*",
      "kms:Revoke*",
      "kms:Disable*",
      "kms:Get*",
      "kms:Delete*",
      "kms:ScheduleKeyDeletion",
      "kms:CancelKeyDeletion",
      "kms:TagResource",
    ]

    resources = ["*"]
  }

  provider = aws.us_east_1
}
