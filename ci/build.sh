#!/usr/bin/env sh

set -ev

OVERRIDES_FILE=ci-overrides.tf

echo '
provider "aws" {
  region     = "string"
}
' > ${OVERRIDES_FILE}

terraform init
AWS_DEFAULT_REGION=us-east-1 terraform validate

rm -rf ci-*.tf*
