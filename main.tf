provider "aws" {
  alias = "us_east_1"
}

locals {
  resource_name_suffix = "${var.route53_zone_id}-${var.project}-${var.environment}-${join("", data.aws_region.current.*.name)}"

  create_new_kms_key = var.enabled && var.encrypt_logs && var.kms_key_arn == ""

  # encrypt_logs = false => key_id = ""
  kms_key_arn = var.enabled &&  var.encrypt_logs && var.kms_key_arn != "" ? var.kms_key_arn : join("", aws_kms_key.default.*.arn)

  cloudwatch_log_group_name = join("", aws_cloudwatch_log_group.default.*.name)
  cloudwatch_log_group_arn  = join("", aws_cloudwatch_log_group.default.*.arn)

  current_aws_account_id = join("", data.aws_caller_identity.current.*.account_id)
}

resource "aws_route53_query_log" "default" {
  count = var.enabled ? 1 : 0

  cloudwatch_log_group_arn = local.cloudwatch_log_group_arn
  zone_id                  = var.route53_zone_id

  depends_on = [aws_cloudwatch_log_resource_policy.default]
}


resource "aws_cloudwatch_log_group" "default" {
  count = var.enabled ? 1 : 0

  name              = "/aws/route53/${join("", data.aws_route53_zone.default.*.name)}-${var.route53_zone_id}"
  retention_in_days = var.dns_log_retention_in_days

  kms_key_id = local.kms_key_arn

  tags = var.tags

  provider = aws.us_east_1
}

resource "aws_cloudwatch_log_resource_policy" "default" {
  count = var.enabled ? 1 : 0

  policy_document = join("", data.aws_iam_policy_document.default.*.json)
  policy_name     = "route53-query-logging-${local.resource_name_suffix}"

  provider = aws.us_east_1
}

data "aws_iam_policy_document" "default" {
  count = var.enabled ? 1 : 0

  statement {
    principals {
      identifiers = ["route53.amazonaws.com"]
      type        = "Service"
    }

    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]

    resources = ["arn:aws:logs:*:*:log-group:/aws/route53/*"]
  }

  provider = aws.us_east_1
}

data "aws_route53_zone" "default" {
  count = var.enabled ? 1 : 0

  zone_id = var.route53_zone_id
}

data "aws_region" "current" {
  count = var.enabled ? 1 : 0
}

data "aws_caller_identity" "current" {
  count = var.enabled ? 1 : 0
}
