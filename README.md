# route53-query-log

Terraform module to setup query logging for Route53.

Route53 logging needs to be collected in a CloudWatch log group that lives in the us-east-1 region.
Therefore this module expects two providers, one main provider that handles most resources and a specific (named) provider that handles the log group resources.
If the module is used in eu-east-1 region then the same provider cna be passed in twice.
See https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/query-logs.html#query-logs-changing-retention-period for the full documentation about logging DNS queries.

## Usage

```hcl-terraform
provider "aws" {
  region = "eu-west-1"
}

provider "aws" {
  region = "us-east-1"

  alias = "us_east_1"
}

data "aws_caller_identity" "current" {}

module "route53_query_log" {
  source = "../../"

  route53_zone_id = aws_route53_zone.test.zone_id

  providers = {
    "aws"           = aws
    "aws.us_east_1" = aws.us_east_1,
  }
}
```
