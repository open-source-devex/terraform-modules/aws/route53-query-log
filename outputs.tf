output "cloudwatch_log_group_id" {
  value = local.cloudwatch_log_group_name
}

output "kms_key_arn" {
  value = local.kms_key_arn
}
